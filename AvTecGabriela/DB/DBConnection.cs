﻿using System;
using System.Data;
using System.Data.SQLite;

namespace AvTecGabriela.DB
{
    public class DBConnection
    {
        private string strConn = @"Data Source=D:\AvTecGabriela\DB\AvTecGabriela.db";

        public void SqlExecute(string pSql)
        {
            using (SQLiteConnection cn = new SQLiteConnection())
            {
                try
                {
                    cn.ConnectionString = strConn;

                    SQLiteCommand cmd = new SQLiteCommand();

                    cmd.Connection = cn;

                    cmd.CommandType = CommandType.Text;

                    cmd.CommandText = pSql;

                    cn.Open();

                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
        }

        public DataTable OpenSQL(string pSql)
        {
            DataTable dt = new DataTable();

            using (SQLiteConnection cn = new SQLiteConnection())
            {
                cn.ConnectionString = strConn;

                SQLiteDataAdapter da = new SQLiteDataAdapter();

                try
                {
                    da.SelectCommand = new SQLiteCommand();

                    da.SelectCommand.Connection = cn;

                    da.SelectCommand.CommandType = CommandType.Text;

                    da.SelectCommand.CommandText = pSql;

                    cn.Open();

                    da.Fill(dt);
                }
                catch (SQLiteException e)
                {
                    throw new Exception(e.ToString());
                }
                finally
                {
                    cn.Close();
                }
            }
            return dt;
        }

        public void InsertPost(int id, int userid, string title, string body)
        {
            string txtSQLQuery = "insert into posts (id, userid, title, body) values (" + id + ", " + userid + ", '" + title + "', '" + body + "')";
            SqlExecute(txtSQLQuery);
        }

        public int SelectTitleFromPost(string value)
        {
            int postId = new int();
            string txtSQLQuery = "select * from posts where title = '" + value + "'";

            DataTable result = OpenSQL(txtSQLQuery);
            DataTableReader resultReader = result.CreateDataReader();

            while (resultReader.Read())
            {
                postId = Int32.Parse(resultReader["id"].ToString());
            }

            return postId;
        }

        public void DeleteAllPosts()
        {
            string txtSQLQuery = "delete from posts";
            SqlExecute(txtSQLQuery);
        }

    }

}
