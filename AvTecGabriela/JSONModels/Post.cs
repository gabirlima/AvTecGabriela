﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AvTecGabriela.JSONModels
{
    public partial class Post
    {
        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }
    }

    public partial class PostList
    {
        public Post[] Posts{ get; set; }
    }

    public partial class Post
    {
        public static Post FromJson(string json) => JsonConvert.DeserializeObject<Post>(json, Converter.Settings);
    }

    public partial class PostList
    {
        public static List<Post> FromJson(string json) => JsonConvert.DeserializeObject<List<Post>>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Post self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
