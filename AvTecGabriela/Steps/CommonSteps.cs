﻿using AvTecGabriela.Endpoints;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace AvTecGabriela.Steps
{
    [Binding]
    public class CommonSteps
    {
        public Common common;

        public CommonSteps(Common common)
        {
            this.common = common;
        }

        [Given(@"the API is up")]
        public void GivenTheAPIIsUp()
        {
            Assert.IsTrue(common.CheckAPI(), "API is out!");
        }

    }
}
