# AvTecnicaGabriela

Projeto para avaliação técnica.

Tecnologias escolhidas:

* Linguagem C#
* Framework .NET
* Framework de testes automatizados: NUnit3
* Framework para BDD: Specflow
* Padrão de escrita de código de testes: PageObjects
* Framework de report de execução de testes: ExtentReport
* Banco de dados: SQLite3

## Arquitetura do projeto

* Feature: Posts.features
* Steps: PostsSteps
* Classes de PageObjects: Common.cs / PostEndpoints.cs
* Classe de JSON Model: PostModel.cs
* Classe para geração do relatório: Hooks.cs
* Classe de conexão e métodos do BD: BDCommection.cs

## Features

* Realiza testes de API para CRUD de posts
* Pesquisa por postId e userId
* Testes positivos e negativos
* Geração do relatório de execução dos testes
* Método que pesquisa todos os posts inclui registros no banco de dados
* Pesquisa por título do post, realizando select no BD, coletando e realizando GET com o ID

## Pré-Requisitos
* Crie no diretório físico "D" a pasta "AvTecGabriela" para geração do relatório
* Possuir instalado Visual Studio 2017 com extensão do Specflow

## Execução
* Baixar aplicação
* Abrir arquivo AvTecGabriela.sln
* Caso gerenciador de testes não esteja aberto, acessar Testes>Janelas>Gerenciador de Testes
* Selecionar opção Executar Tudo - ao todo existem 17 testes

### Observações
Infelizmente não consegui finalizar o build no Jenkins, mas o projeto possui o arquivo Jenkisfile do que estava em andamento.

Qualquer dúvida, estou à disposição.

Gabriela Lima